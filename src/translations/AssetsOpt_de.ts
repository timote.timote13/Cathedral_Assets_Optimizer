<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>MainWindow</name>
    <message>
        <source>Cathedral Assets Optimizer</source>
        <translation>Cathedral Assets Optimizer</translation>
    </message>
    <message>
        <source>Will write which files would be affected, without actually processing them</source>
        <translation>Protokolliert welche Veränderungen durchgeführt werden würden ohne die Dateien tatsächlich zu bearbeiten.</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>Verzeichnis öffnen</translation>
    </message>
    <message>
        <source>Run</source>
        <translation>Los</translation>
    </message>
    <message>
        <source>One mod</source>
        <translation>Einzel Mod&lt;br&gt;</translation>
    </message>
    <message>
        <source>Dry run</source>
        <translation>Trocken-Durchlauf</translation>
    </message>
    <message>
        <source>Show advanced settings</source>
        <translation>Erweiterte Einstellungen anzeigen</translation>
    </message>
    <message>
        <source>SSE</source>
        <translation>SSE</translation>
    </message>
    <message>
        <source>BSA</source>
        <translation>BSA</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Extracts any BSAs present in the mod folder. &lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Extrahiert alle BSA Archive die in dem Mod Ordner vorhanden sind. &lt;span style=&quot; font-weight:600;&quot;&gt; Warnung&lt;/span&gt;. Wenn du diese Option aktivierst, wird die Verarbeitungszeit deutlich länger.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Extract BSA</source>
        <translation>BSA extrahieren</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, a backup of existing bsa is created. Enabling this option will disable those backups.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Standardmäßig wird ein Back-up von jedem bereits vorhandenen BSA Archiv erstellt. Wenn du diese Option deaktivierst wenn diese Back-ups deaktiviert.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Delete backups </source>
        <translation>Datensicherungen löschen</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt;Creates a new BSA, packing the existing loose files. &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt; erstellt eine Neues BSA Archiv, und das existierende Datei hinein gepackt werden. &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt; Warnung&lt;/span&gt;. Wenn du diese Option aktivierst, wird die Verarbeitungszeit deutlich erhöht.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create BSA</source>
        <translation>BSA erstellen</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Erweitert</translation>
    </message>
    <message>
        <source>Morrowind</source>
        <translation type="vanished">Morrowind</translation>
    </message>
    <message>
        <source>Oblivion</source>
        <translation type="vanished">Oblivion</translation>
    </message>
    <message>
        <source>Skyrim / Fallout 3 / Fallout New Vegas</source>
        <translation type="vanished">Skyrim / Fallout 3 / Fallout New Vegas</translation>
    </message>
    <message>
        <source>Fallout 4</source>
        <translation type="vanished">Fallout 4</translation>
    </message>
    <message>
        <source>Maximum size</source>
        <translation>Maximale Größe</translation>
    </message>
    <message>
        <source>Extension</source>
        <translation type="vanished">Dateierweiterung</translation>
    </message>
    <message>
        <source>Suffix</source>
        <translation type="vanished">Suffix</translation>
    </message>
    <message>
        <source>Meshes</source>
        <translation>Polygonnetz</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Attempts to repair meshes which are guaranteed to crash the game. Headparts are included.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Versucht Polygonnetze zu reparieren die garantiert das Spiel crashen werden. Headparts gehören dazu.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Necessary optimization</source>
        <translation>Notwendige Optimierungen</translation>
    </message>
    <message>
        <source>Medium optimization</source>
        <translation>Mittlere Optimierungen</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fully optimize all meshes. Only apply if standard mesh optimization ignored necessary files. May lower visual quality.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Optimiert alle Polygonnetze vollständig. Benutze diese Option nur wenn die Standardoptimierungen wichtige Dateien übersehen haben. Kann die Qualität verschlechtern.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Full optimization</source>
        <translation>Vollständige Optimierungen</translation>
    </message>
    <message>
        <source>Always process headparts</source>
        <translation>Headparts immer optimieren</translation>
    </message>
    <message>
        <source>Resave meshes</source>
        <translation>Polygonnetze überschreiben</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Oblivion (V20_0_0_5)</source>
        <translation>Oblivion (V20_0_0_5)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (V20_2_0_7)</source>
        <translation>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (V20_2_0_7)</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <source>Oblivion (11)</source>
        <translation>Oblivion (11)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (12)</source>
        <translation>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (12)</translation>
    </message>
    <message>
        <source>Stream</source>
        <translation>Streamen</translation>
    </message>
    <message>
        <source>Oblivion / Fallout 3 (82)</source>
        <translation>Oblivion / Fallout 3 (82)</translation>
    </message>
    <message>
        <source>Skyrim (83)</source>
        <translation>Skyrim (83)</translation>
    </message>
    <message>
        <source>Skyrim SE (100)</source>
        <translation>Skyrim SE (100)</translation>
    </message>
    <message>
        <source>Fallout 4 (130)</source>
        <translation>Fallout 4 (130)</translation>
    </message>
    <message>
        <source>Textures</source>
        <translation>Texturen</translation>
    </message>
    <message>
        <source>Resizing</source>
        <translation type="vanished">Auflösung verändern</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>Höhe:</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>Breite:</translation>
    </message>
    <message>
        <source>By ratio</source>
        <translation>Mit der Kompressionsrate</translation>
    </message>
    <message>
        <source>By fixed size</source>
        <translation>Mit fester Dateigröße</translation>
    </message>
    <message>
        <source>TGA</source>
        <translation>TGA</translation>
    </message>
    <message>
        <source>Enable TGA conversion</source>
        <translation>Aktiviere TGA Konvertierung</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <source>New profile</source>
        <translation>Neues Profil</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will list all files that would be modified, without actually processing them. Will ignore files in BSAs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Protokolliert mit Ausnahme von in BSA gespeicherten Dateien welche Veränderungen durchgeführt werden würden ohne die Dateien tatsächlich zu bearbeiten.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Several mods </source>
        <translation>Mehrere Mods</translation>
    </message>
    <message>
        <source>Process BSAs</source>
        <translation type="vanished">BSAs Verarbeiten</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The maximum uncompressed size of the files in GB. Increasing it will increase the number of files stored per BSA, but you might reach the hard limit of BSA size, causing the game to crash.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; die maximale und komprimierter Größe aller Dateien die zum Archiv hinzugefügt werden sollen in Gigabyte. Wenn du diese Nummer erhöhst wird die Anzahl der Dateien die in einem BSA Archiv gespeichert werden erhöht, solltest du allerdings die maximale Größe erreichen bitte Spiel abstürzen. Das ist besonders wichtig für alle Titel außer Fallout 4&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The extension of the bsa. Usually, this will be &amp;quot;.bsa&amp;quot; for Skyrim, &amp;quot;.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; die Dateierweiterung des BSA Archives. Normalerweise ist das &amp;quot;.bsa&amp;quot; für Skyrim, &amp;quot;.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Main.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Beispiel: &amp;quot; - Main.ba2&amp;quot; für Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create textures BSA</source>
        <translation type="vanished">Texturen BSA erstellen</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Textures.bsa&amp;quot; for Skyrim SE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Beispiel: &amp;quot; - Textures.bsa&amp;quot; für Skyrim SE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Expert</source>
        <translation>Experte</translation>
    </message>
    <message>
        <source>BSA Format</source>
        <translation type="vanished">BSA Format</translation>
    </message>
    <message>
        <source>Textures BSA Format</source>
        <translation type="vanished">Texturen BSA Format</translation>
    </message>
    <message>
        <source>Process meshes</source>
        <translation>Polygonnetze verarbeiten</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Perform necessary optimization, and also lightly optimizes other meshes. This may fix some visual issues, but may also lower quality. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Führt notwendige Optimierungen aus, andere Polygonnetze werden ebenfalls leicht optimiert. Das kann möglicherweise Grafikfehler beheben, kann allerdings auch die Qualität verschlechtern. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When this option is disabled, headparts are ignored. It is recommended to keep it disabled when processing several mods.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Wenn diese Option deaktiviert es werden headparts ignoriert. Es ist empfohlen es deaktiviert zu lassen wenn mehrere Mods auf einmal verarbeitet werden.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will resave meshes even if optimization is disabled. Will perform the same optimization as opening a mesh in NifSkope.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Öffnet alle Polygonnetze und speichert sie selbst dann wenn Optimierungen deaktiviert sind. Dies für die selben Bereinigungen aus, wie wenn du es in NifSkope öffnen und speichern würdest. Manche Nutzer haben berichtet dass dadurch manche Polygonnetze transparent wurden. In der Regel ist diese Option allerdings sicher.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Process textures</source>
        <translation>Texturen verarbeiten</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts any TGA files into DDS, as SSE cannot read these. Attempts to convert and fix any textures that would crash the game, as some older formats are incompatible with SSE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Versucht alle Texturen in Formaten, die nicht unterstützt werden und das Spiel zum Absturz bringen würden, in eines zu konvertieren das das Spiel unterstützt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Compresses uncompressed textures to the configured output format. It is recommended for SSE and FO4, but may highly decrease quality for other games.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Konvertiert unkomprimierte Texturen in das ausgewählte Format. Stark fallen für SSE und FO4, Kann die Qualität in anderen Spielen deutlich verschlechtern wenn Formate wie BC1, BC2 oder BC3 ausgewählt werden.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Compress textures</source>
        <translation>Texturen komprimieren.</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Generates mipmaps for textures, improving the performance at the cost of higher disk and vram usage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Generiert mipmaps für Texturen, verbessert die Leistung, erhöht allerdings auch die Festplattennutzung und VRAM Benutzung.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Generate mipmaps</source>
        <translation>Mipmaps generieren</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes textures, dividing the current width and height by the values given. Must result in texture dimensions expressed in powers of two, no less than 4x4. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Verändert die Größe von Texturen, in dem sie die aktuelle Größe durch den Faktor den du angegeben hast dividiert. Die End-Auflösung muss eine zweier Potenz sein und mindestens 4 × 4 Pixel groß sein. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes texture dimensions to the given sizes. Must result in texture dimensions expressed in powers of two, no less than 4x4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; veränderte Auflösung von Texturen zu der spezifizierten Größe. Die minimale Größe ist 4 × 4 Pixel. Stelle sicher dass die Auflösung zwei hoch X ist. Valide Auflösungen für x und y sind beispielsweise 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 und 8192. Texturen werden ausschließlich verkleinert, sollte die Größe kleiner oder gleich sein als die ausgewählte Größe wie die Textur nicht bearbeitet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Some games do not support compressed interface textures.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; manche Spiele unterstützen keine komprimierten Benutzeroberflächen Texturen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Output format for uncompressed textures, modified textures, and converted TGA files. BC7 should only be used for SSE or FO4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Ausgabeformat vier und komprimierte Texturen, modifizierte Texturen, und konvertierte TGA Texturen. BC7 sollte benutzt werden wenn das Resultat für SSE oder FO4 gedacht ist.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Output format</source>
        <translation>Ausgabeformat</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will convert TGAs to a compatible DDS format if option is selected.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Konvertiert TGA Texturen in ein kompatibles DDS Textur Format wenn du diese Option auswählst.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>BC7 (BC7_UNORM)</source>
        <translation>BC7 (BC7_UNORM)</translation>
    </message>
    <message>
        <source>BC3 (BC3_UNORM)</source>
        <translation>BC3 (BC3_UNORM)</translation>
    </message>
    <message>
        <source>BC1 (BC1_UNORM)</source>
        <translation>BC1 (BC1_UNORM)</translation>
    </message>
    <message>
        <source>Uncompressed (R8G8B8A8_UNORM)</source>
        <translation>Unkomprimiert (R8G8B8A8_UNORM)</translation>
    </message>
    <message>
        <source>Process animations</source>
        <translation>Animationen verarbeiten</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts animations to the appropriate format. If an animation is already compatible, no change will be made.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Konvertiert Animation in das passende Format. Wenn die Animation bereits kompatibel ist werden keine Veränderungen vorgenommen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <source>Enable debug log</source>
        <translation>Debugg Protokoll aktivieren</translation>
    </message>
    <message>
        <source>Used when reporting bugs</source>
        <translation>Wird benutzt wenn man Probleme Berichten will</translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation>Dokumentation</translation>
    </message>
    <message>
        <source>Discord</source>
        <translation>Discord</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>Über Qt</translation>
    </message>
    <message>
        <source>Show tutorials</source>
        <translation>Anleitungen anzeigen</translation>
    </message>
    <message>
        <source>Open log file</source>
        <translation>Protokoll Datei öffnen</translation>
    </message>
    <message>
        <source>Interface</source>
        <translation>Oberfläche</translation>
    </message>
    <message>
        <source>Compress interface textures</source>
        <translation>Oberflächen Texturen komprimieren</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Textures using these formats will be converted to a supported format.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Texturen die eines dieser Formate verwenden werden in ein unterstütztes Format konvertiert.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Unwanted formats</source>
        <translation>Ungewollte Formate</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <source>Animations</source>
        <translation>Animationen</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>Protokoll</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation>Werkzeuge</translation>
    </message>
    <message>
        <source>Enable dark theme</source>
        <translation>Dunkles Design aktivieren</translation>
    </message>
    <message>
        <source>Advanced settings</source>
        <translation>Erweiterte Einstellungen</translation>
    </message>
    <message>
        <source>Advanced settings can only be modified when using custom profiles.</source>
        <translation>Erweiterte Einstellungen kann nur modifiziert werden wenn ein benutzerdefiniertes Profil benutzt wird.</translation>
    </message>
    <message>
        <source>Several mods option</source>
        <translation>Mehrere Mods Option</translation>
    </message>
    <message>
        <source>You have selected the several mods option. This process may take a very long time, especially if you process BSA. </source>
        <translation>Du hast du mehrere Mods Option ausgewählt. Dieser Prozess kann sehr lange dauern, besonders dann wenn du BSA Dateien verarbeiten möchtest.</translation>
    </message>
    <message>
        <source>This process has only been tested on the Mod Organizer mods folder.</source>
        <translation>Dieser Prozess wurde nur an Matt Organizer Mod Ordnern ausprobiert.</translation>
    </message>
    <message>
        <source>
Made by G&apos;k
This program is distributed in the hope that it will be useful but WITHOUT ANY WARRANTLY. See the Mozilla Public License</source>
        <translation>
Programmiert von G&apos;k
Dieses Programm wird verteilt der Hoffnung dass es nützlich ist aber OHNE GEWÄHRLEISTUNG. Siehe die Mozilla Public Lizenz.</translation>
    </message>
    <message>
        <source>Save unsaved changes</source>
        <translation type="vanished">Ungespeicherte Änderungen speichern</translation>
    </message>
    <message>
        <source>You have unsaved changes. Do you want to save them? You can also press &apos;Yes to all&apos; to always save unsaved changes</source>
        <translation type="vanished">Du hast umgespeicherte Änderungen. Willst du diese speichern? Du kannst auch &apos;ja zu allen&apos; sagen um immer ungespeicherte Änderung zu speichern.</translation>
    </message>
    <message>
        <source>You are about to create a new profile. It will create a new directory in &apos;CAO/profiles&apos;. Please check it out after creation, some files will be created inside it.</source>
        <translation>Du bist gerade dabei ein neues Profil zu erstellen. Es wird einen neuen Ordner in &apos;CAO/profiles&apos; erstellen. Bitte schaust dir an nachdem es erstellt wurde, einige Dateien sollten darin erstellt worden sein.</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <source>Base profile</source>
        <translation>Grundprofil</translation>
    </message>
    <message>
        <source>Which profile do you want to use as a base?</source>
        <translation>Welches Profil würdest du gerne als Grundprofil benutzen?</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>An exception has been encountered and the process was forced to stop: </source>
        <translation>Ein Fehler ist aufgetreten und der Prozess wurde beendet:</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>Fertig</translation>
    </message>
    <message>
        <source>Welcome to %1 %2</source>
        <translation>Willkommen zum %1 %2</translation>
    </message>
    <message>
        <source>It appears you are running CAO for the first time. All options have tooltips explaining what they do. If you need help, you can also join us on Discord. A dark theme is also available.</source>
        <translation>Es sieht so aus als würdest du CAO zum ersten Mal ausführen. Alle Optionen haben einen Tooltip das dir erklärt was sie tut. Fahre einfach mit dem Mauszeiger über eine Option um die Erklärung anzuzeigen. Falls du Hilfe brauchst, kannst du uns auch auf unserem Discord besuchen. Ein dunkles Design ist auch verfügbar.</translation>
    </message>
    <message>
        <source>Create dummy plugins</source>
        <translation>Platzhalter Plugins erstellen</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;There are three types of BSAs CAO can create : uncompressible, textures and standard. The uncompressible one contains sounds. If this option is checked, these three types of BSAs will be merged if possible.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; gibt drei Arten von BSAs ist die CAO erstellen kann : unkomprimiert, Touren und Standard. Unkomprimierte beispielsweise enthalten Ton Dateien. Wenn diese Option ausgewählt ist, werden diese drei Arten von BSAs falls möglich zusammengefügt .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create the least BSAs possible</source>
        <translation type="vanished">Erstelle die minimal mögliche Anzahl an BSAs</translation>
    </message>
    <message>
        <source>Save UI</source>
        <translation type="vanished">Benutzeroberflächeneinstellungen speichern</translation>
    </message>
    <message>
        <source>Base</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Extra</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Whether CAO will delete the files it packed into BSAs or leave them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete source files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Skyrim LE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FO4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Allow compression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dummy plugins are almost always necessary. Keep this checked unless you know what you are doing&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Compressed BSAs use less space on disk, but they might perform slightly worse in game&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;There are three types of BSAs CAO can create : incompressible, textures and standard. &lt;/p&gt;&lt;p&gt;If enabled, this option will always &lt;span style=&quot; font-weight:600;&quot;&gt;separate textures into their own BSAs&lt;/span&gt;. Otherwise, they will be merged into the standard one if possible.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Separate textures BSA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;There are three types of BSAs CAO can create : incompressible, textures and standard. &lt;/p&gt;&lt;p&gt;If enabled, this option will always &lt;span style=&quot; font-weight:600;&quot;&gt;separate incompressible files into their own BSAs&lt;/span&gt;. Otherwise, they will be merged into the standard one if possible.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Separate incompressible BSA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Downsizing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Recommended&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:400;&quot;&gt;Mipmaps are smaller copies of textures embedded in the same file which get used when things are small or far away and a pixel on screen covers several pixels in the texture. &lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:400;&quot;&gt;They improve visual quality with a small performance cost.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TexturesFormatSelectDialog</name>
    <message>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
</context>
</TS>
