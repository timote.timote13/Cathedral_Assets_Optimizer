cmake_minimum_required(VERSION 3.12...3.22)

project(Cathedral_Assets_Optimizer VERSION 5.3.14 LANGUAGES CXX)

add_subdirectory(src)
